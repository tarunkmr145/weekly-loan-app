<?php

use App\Http\Controllers\API\LoanController;
use App\models\Loan_emi_detail;

if(!function_exists('deduct_loan')){
    function deduct_loan($loan_id,$user_id,$amount_paid,$next_emi_date){
        Loan_emi_detail::create([
            'loan_id' => $loan_id,
            'user_id' => $user_id,
            'amount_paid' => $amount_paid,
            'next_emi_date'  => $next_emi_date,
            'status' => 1
        ]);
    }
}