<?php

namespace App\Http\Controllers\API;

use App\models\Loan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\LoanEmiDetail;
use Illuminate\Support\Facades\Validator;

class LoanController extends Controller
{
    public function deductLoan(Request $request){
        try{
            //Validation starts here
            $validator = Validator::make($request->all(), [
                'loan_amount' => 'required|integer',
                'loan_term' => 'required|integer',
            ]);
    
            if ($validator->fails()) {
                $responseArray= [
                    'code' => 400,
                    'message' => 'failed',
                    'reason' => $validator->errors()
                ];
                return response()->json($responseArray);
            }
            //validation ends here
            
            $user_id = auth()->id();
            $loan_amount = $request->loan_amount;
            $loan_term = $request->loan_term;
            $loan_issue_date = date('Y-m-d');
            $next_emi_date =  date('Y-m-d', strtotime($loan_issue_date. ' + 7 days'));
    
            $amount_paid = $loan_amount/$loan_term;
            $pending_amount = $loan_amount - $amount_paid;
            
            //Insert data for loan table 
            $loan = Loan::create([
                'user_id' => $user_id,
                'loan_amount' => $loan_amount,
                'loan_term' => $loan_term,
                'loan_issue_date' => $loan_issue_date,
                'pending_amount' => $pending_amount,
                'next_emi_date' => $next_emi_date
            ]);

            //Insert data for Loan emi detail table
            if($loan){
                LoanEmiDetail::create([
                    'loan_id' => $loan->id,
                    'user_id' => $loan->user_id,
                    'amount_paid' => $amount_paid,
                    'status' => 1
                ]);
                $data['loan_amount'] = $loan_amount;
                $data['amount_paid'] = $amount_paid;
                $data['pending_amount'] = $pending_amount;  
                $data['loan_issue_date'] = $loan_issue_date;  
                $data['next_emi_date'] = $next_emi_date;
        
                return response()->json([
                    'code' => 200,
                    'message' => 'EMI Deducted Successfully!',
                    'data' => $data
                ]);
            }

            return response()->json([
                'code' => 200,
                'message' => 'Something went wrong!',
            ]);
      
        }
        
        catch(\Exception $e){
            $responseArray= [
                'code' => 400,
                'message' => $e->getMessage()
            ];
            return response()->json($responseArray);
        }
        
    }
}
