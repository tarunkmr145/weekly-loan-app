<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class LoanEmiDetail extends Model
{
    protected $fillable = ['loan_id','user_id','amount_paid','next_emi_date'];

    public function Loan(){
        return $this->belongsTo(Loan::class);
    }
}
