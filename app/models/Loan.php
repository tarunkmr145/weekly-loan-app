<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $fillable = ['user_id','loan_amount','loan_term','loan_issue_date','status','next_emi_date'];

    public function Loan_emi_details(){
        return $this->hasMany(LoanEmiDetail::class);
    }

    public function getNextEmiDate(){
        $today = date('Y-m-d');
        return date('Y-m-d', strtotime($today. ' + 7 days'));
    }
}
