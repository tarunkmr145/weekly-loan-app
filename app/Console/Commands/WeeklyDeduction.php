<?php

namespace App\Console\Commands;

use App\models\Loan;
use App\models\LoanEmiDetail;
use Illuminate\Console\Command;

class WeeklyDeduction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weekly:deduction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will deduct the loan installment weekly!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {    
        //Custom command to deduct the EMI on weekly basis for all the users
        $today = date('Y-m-d');
        $pendingLoanDetails = Loan::where('next_emi_date','=',$today)->where('status',0)->withCount('Loan_emi_details')->get();
        if(!empty($pendingLoanDetails)){
            foreach($pendingLoanDetails as $pendingLoanInfo){
                $loan_status =  0;
                $next_emi_date =  date('Y-m-d', strtotime($today. ' + 7 days'));
                $emi_amount = $pendingLoanInfo->loan_amount / $pendingLoanInfo->loan_term;
                //Check if emi details count from loan emi table is lesser than loan term
                if($pendingLoanInfo->loan_emi_details_count < $pendingLoanInfo->loan_term){
                        LoanEmiDetail::create([
                            'loan_id' => $pendingLoanInfo->id,
                            'user_id' => $pendingLoanInfo->user_id,
                            'amount_paid' => $emi_amount,
                            'status' => 1
                        ]);

                        if(($pendingLoanInfo->loan_emi_details_count + 1)  == ($pendingLoanInfo->loan_term)){
                            $next_emi_date =  null;
                            $loan_status =  1;
                        }
                        Loan::where('id',$pendingLoanInfo->id)->update(['next_emi_date'=>$next_emi_date,'status' => $loan_status]);
                }
            }
        }
    }
}
